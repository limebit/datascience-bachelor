# Big Data & Data Science
(FOM Bachelorstudiengang "Wirtschaftsfinformatik", Dozent P. Koch, M. Sc.)
___

# Willkommen im Bitbucket Repository zur Vorlesung "Big Data & Data Science"

*Bitte beachten Sie, dass das Repository kostanten Erweiterungen unterworfen ist, weitere Inhalte werden im Laufe des Semesters bereitgestellt.*

**Nutzung des Repository als Forum bei Fragen zu Implementierung:**

- Sollten Sie Schwierigkeiten bei der Implementierung Ihrer Seminararbeit haben können Sie jederzeit alle entsprechenden Fragen und Probleme unter "Issues" stellen.
- Innerhalb kurzer Zeit wird Ihr Problem dann beantwortet.
- Bitte beachten Sie, dass jeweils 5 Tage vor Vorstellung Ihrer Seminararbeit und vor Abgabe Ihrer Seminararbeit keine Fragen mehr beantwortet werden
	- Diese Deadline soll verhindern, dass Fragen von Studierenden unfairerweise unbeantwortet bleiben, weil Sie "Last Minute" gestellt werden
	- Alle Fragen die vor Ablauf der jeweiligen Fristen gestellt werden, werden garantiert rechtzeitig beantwortet
	- Fragen nach Ablauf der Frist werden nicht mehr bearbeitet


**Weitere Themen im Rahmen der Vorlesung:**

- Einführung Python


